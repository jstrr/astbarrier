from datetime import datetime, timedelta
from flask import Flask, request, session
import flask.ext.admin as admin
from flask.ext.admin import helpers as admin_helpers
from flask.ext.admin.contrib.sqla import ModelView, filters
from flask.ext.babelex import Babel, gettext, lazy_gettext
from flask.ext.mail import Mail, Message
from flask.ext.security import Security, SQLAlchemyUserDatastore, login_required
from flask.ext.migrate import Migrate
from models import db, add_log
import models


app = Flask(__name__)
db.init_app(app)

app.config.from_object('config')
try:
    app.config.from_pyfile('instance/config.py')
except IOError:
    pass

mail = Mail(app)

babel = Babel(app)
@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', app.config.get('DEFAULT_LANGUAGE'))

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
security = Security(app, user_datastore)

migrate = Migrate(app, db)


@app.route('/check_number/<number>')
@app.route('/check_number/<number>/<barrier>')
def check_number(number, barrier=None):
    message = ''
    if barrier:
        message += gettext('Barrier: %(barrier)s. ', barrier=barrier)
    # Refuse unknown numbers
    rec = models.WhiteList.query.filter_by(number=number).first()
    if not rec:
        # No such number
        message += lazy_gettext(
            'Entry request from unknown number %(number)s: refused.', number=number)
        add_log(message, number=number,is_accepted=False)
        return 'NO'

    # Now check banned
    ban = models.Ban.query.filter_by(number=number).first()
    if ban:
        if (datetime.now() - ban.added).seconds < app.config['BAN_RULES']['period']:
            message += lazy_gettext('Request refused - number is banned.')
            add_log(message, number=number,is_accepted=False)
            return 'BAN'
        else:
            # Remove ban and permit
            db.session.delete(ban)
            db.session.commit()
            message += lazy_gettext('Number has been unbanned, request is accepted.')
            add_log(message, number=number,is_accepted=True)
            return 'YES'

    # Now check recent access and probably ban
    recent = datetime.now() - timedelta(0, app.config['BAN_RULES']['interval'])
    log_count = models.Logbook.query.filter_by(number=number).filter(models.Logbook.created > recent).count()
    if log_count > app.config['BAN_RULES']['calls']:
        ban = models.Ban(number=number)
        db.session.add(ban)
        db.session.commit()
        period = app.config['BAN_RULES']['period'] # lazy_gettext cannot take it from below
        add_log(message + lazy_gettext('Number banned for %(period)s seconds', period=period),
                number=number)
        return 'BAN'

    # All is clear
    message += lazy_gettext('Entry request from %(number)s - accepted.', number=rec)
    add_log(message, number=number, is_accepted=True)
    return 'YES'


admin = admin.Admin(name=lazy_gettext('Barrier'), template_mode='bootstrap3',
                    base_template='my_master.html', url='/')

app.admin = admin
from views import add_admin_views
add_admin_views(app)
admin.init_app(app)


@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers
        )
