"""empty message

Revision ID: 1608b174f3b1
Revises: 2ba804601b9
Create Date: 2015-09-29 14:11:05.584885

"""

# revision identifiers, used by Alembic.
revision = '1608b174f3b1'
down_revision = '2ba804601b9'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('white_list', sa.Column('car', sa.String(length=255), nullable=True))
    op.add_column('white_list', sa.Column('flat', sa.String(length=100), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('white_list', 'flat')
    op.drop_column('white_list', 'car')
    ### end Alembic commands ###
