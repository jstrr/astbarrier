#!/usr/bin/env python

from datetime import date
from flask.ext.script import Manager, Shell
from flask.ext.babelex import lazy_gettext
from flask.ext.mail import Mail, Message
from flask.ext.security import utils
from flask.ext.migrate import MigrateCommand
from app import app, db, mail, user_datastore
from models import Logbook

manager = Manager(app)

@manager.command
def populate():
    user_datastore.create_role(name='admin')
    user_datastore.create_role(name='manager')
    user_datastore.create_role(name='analytics')
    
    encrypted_password = utils.encrypt_password('1234')
    user_datastore.create_user(email='admin@email.com',
                               password=encrypted_password)
    user_datastore.create_user(email='manager@email.com',
                               password=encrypted_password)
    user_datastore.create_user(email='analytics@email.com',
                               password=encrypted_password)
    user_datastore.add_role_to_user('admin@email.com', 'admin')
    user_datastore.add_role_to_user('admin@email.com', 'manager')
    user_datastore.add_role_to_user('admin@email.com', 'analytics')
    user_datastore.add_role_to_user('manager@email.com', 'manager')
    user_datastore.add_role_to_user('manager@email.com', 'analytics')
    user_datastore.add_role_to_user('analytics@email.com', 'analytics')

    db.session.commit()


@manager.command
def daily_report():
    logs = models.Logbook.query.filter(models.Logbook.created > date.today()).all()
    msg = Message(
            lazy_gettext('[Barrier] Daily report for %s' % date.today()),
            sender=app.config['MAIL_USERNAME'],
            recipients=app.config['REPORT_SEND_TO']
            )
    msg.body = ''
    for log in logs:
        msg.body += '%s: %s\n' % (log.created, log.message)
    mail.send(msg)


def _make_context():
    return dict(app=app, db=db, models=models)
manager.add_command("shell", Shell(make_context=_make_context))


manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    #host=app.config['HOST'], port=app.config['PORT']
    manager.run()

