import os
import sys

from flask_collect import Collect
from app import app

app.config['COLLECT_STATIC_ROOT'] = sys.argv[1]

collect = Collect()
collect.init_app(app)

if __name__ == '__main__':
    collect.collect(verbose=True)
